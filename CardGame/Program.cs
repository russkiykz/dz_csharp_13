﻿using System;
using System.Collections.Generic;

namespace CardGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();
            Console.WriteLine($"Добро пожаловать в Карточную игру!\n\nДля продолжения нажмите клавишу...");
            Console.ReadLine();
            Console.Clear();
            game.GameProcess();
        }
    }
}
