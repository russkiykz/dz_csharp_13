﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardGame
{
    public class Player
    {
        public List<Karta> CardsInHand { get; set; }

        public Player()
        {
            CardsInHand = new List<Karta>();
        }

        public void CardWithdrawal()
        {
            foreach(var card in CardsInHand)
            {
                Console.WriteLine($"{card.CardType} [{card.CardSuit}]");
            }
        }
    }
}
