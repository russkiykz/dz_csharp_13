﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardGame
{
    public class Game
    {
        public List<Player> Players { get; set; }
        public List<Karta> CardDeck { get; set; }

        public Game()
        {
            Players = new List<Player>();
            Players.Add(new Player());
            Players.Add(new Player());
            CardDeck = new List<Karta>();
            CardDeck.Add(new Karta("6", "Heart"));
            CardDeck.Add(new Karta("7", "Heart"));
            CardDeck.Add(new Karta("8", "Heart"));
            CardDeck.Add(new Karta("9", "Heart"));
            CardDeck.Add(new Karta("10", "Heart"));
            CardDeck.Add(new Karta("Jack", "Heart"));
            CardDeck.Add(new Karta("Queen", "Heart"));
            CardDeck.Add(new Karta("King", "Heart"));
            CardDeck.Add(new Karta("Ace", "Heart"));
            CardDeck.Add(new Karta("6", "Diamond"));
            CardDeck.Add(new Karta("7", "Diamond"));
            CardDeck.Add(new Karta("8", "Diamond"));
            CardDeck.Add(new Karta("9", "Diamond"));
            CardDeck.Add(new Karta("10", "Diamond"));
            CardDeck.Add(new Karta("Jack", "Diamond"));
            CardDeck.Add(new Karta("Queen", "Diamond"));
            CardDeck.Add(new Karta("King", "Diamond"));
            CardDeck.Add(new Karta("Ace", "Diamond"));
            CardDeck.Add(new Karta("6", "Club"));
            CardDeck.Add(new Karta("7", "Club"));
            CardDeck.Add(new Karta("8", "Club"));
            CardDeck.Add(new Karta("9", "Club"));
            CardDeck.Add(new Karta("10", "Club"));
            CardDeck.Add(new Karta("Jack", "Club"));
            CardDeck.Add(new Karta("Queen", "Club"));
            CardDeck.Add(new Karta("King", "Club"));
            CardDeck.Add(new Karta("Ace", "Club"));
            CardDeck.Add(new Karta("6", "Spade"));
            CardDeck.Add(new Karta("7", "Spade"));
            CardDeck.Add(new Karta("8", "Spade"));
            CardDeck.Add(new Karta("9", "Spade"));
            CardDeck.Add(new Karta("10", "Spade"));
            CardDeck.Add(new Karta("Jack", "Spade"));
            CardDeck.Add(new Karta("Queen", "Spade"));
            CardDeck.Add(new Karta("King", "Spade"));
            CardDeck.Add(new Karta("Ace", "Spade"));
        }

        public void ShufflingCards()
        {
            Random random = new Random();
            for (int i = 0; i < CardDeck.Count; i++)
            {
                var temp = CardDeck[i];
                CardDeck.RemoveAt(i);
                CardDeck.Insert(random.Next(CardDeck.Count), temp);
            }
        }

        public void DealOfCards()
        {
            for (int i = 0; i < CardDeck.Count; i++)
            {
                if (i % 2==0)
                {
                    Players[0].CardsInHand.Add(CardDeck[i]);
                }
                else
                {
                    Players[1].CardsInHand.Add(CardDeck[i]);
                }
            }
        }

        public void GameProcess()
        {
            ShufflingCards();
            DealOfCards();
            while (true)
            {
                if (Players[0].CardsInHand.Count == 0 || Players[1].CardsInHand.Count == 0)
                {
                    break;
                }
                Console.Write("\nПервый игрок: ");
                Console.WriteLine($"{Players[0].CardsInHand[0].CardSuit} {Players[0].CardsInHand[0].CardType}");
                Console.Write("Второй игрок: ");
                Console.WriteLine($"{Players[1].CardsInHand[0].CardSuit} {Players[1].CardsInHand[0].CardType}");
                if (Players[0].CardsInHand[0].CardPoint >= Players[1].CardsInHand[0].CardPoint)
                {
                    Console.WriteLine("\n*Взятку взял первый игрок*");
                    Players[0].CardsInHand.Add(Players[0].CardsInHand[0]);
                    Players[0].CardsInHand.RemoveAt(0);
                    Players[0].CardsInHand.Add(Players[1].CardsInHand[0]);
                    Players[1].CardsInHand.RemoveAt(0);
                }
                else
                {
                    Console.WriteLine("\n*Взятку взял второй игрок*");
                    Players[1].CardsInHand.Add(Players[1].CardsInHand[0]);
                    Players[1].CardsInHand.RemoveAt(0);
                    Players[1].CardsInHand.Add(Players[0].CardsInHand[0]);
                    Players[0].CardsInHand.RemoveAt(0);
                }
                Console.WriteLine($"\nКоличество карт: \nПервый игрок: {Players[0].CardsInHand.Count} Второй игрок: {Players[1].CardsInHand.Count}\n\nДля продолжения нажмите клавишу...");
                Console.ReadLine();
                Console.Clear();
            }
            if (Players[0].CardsInHand.Count == 36)
            {
                Console.WriteLine("Победил первый игрок");
            }
            else
            {
                Console.WriteLine("Победил второй игрок");
            }
            Console.WriteLine("\nКонец игры");
        }
    }
}
