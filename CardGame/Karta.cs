﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CardGame
{
    public class Karta
    {
        public string CardSuit { get; set; }
        public string CardType { get; set; }
        public int CardPoint { get; set; }

        public Karta(string cardType, string cardSuit)
        {
            CardType = cardType;
            CardSuit = cardSuit;
            switch (cardType)
            {
                case "6":
                    CardPoint = 6;
                    break;
                case "7":
                    CardPoint = 7;
                    break;
                case "8":
                    CardPoint = 8;
                    break;
                case "9":
                    CardPoint = 9;
                    break;
                case "10":
                    CardPoint = 10;
                    break;
                case "Jack":
                    CardPoint = 11;
                    break;
                case "Queen":
                    CardPoint = 12;
                    break;
                case "King":
                    CardPoint = 13;
                    break;
                case "Ace":
                    CardPoint = 14;
                    break;
            }
        }
    }
}
